package com.user.userauthentication.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.user.userauthentication.service.UserService;
import com.user.userauthentication.user.User;

@RestController
@RequestMapping("/user")
@CrossOrigin("http://localhost:3000")
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping("/adduser")
	public User addUser(@RequestBody User users) {
		return userService.save(users);
	}

	@GetMapping("/getuser")
	public String findByUsernameAndPassword(String username,String password){
		List<User> user= userService.findByUsernameAndPassword(username, password);
		String isPresent="";
		  if(user.isEmpty()) { 
			  isPresent="Invalid username or password"; 
			  }else {
				  isPresent="Login successful"; }
		 
		return isPresent;
	}

}
